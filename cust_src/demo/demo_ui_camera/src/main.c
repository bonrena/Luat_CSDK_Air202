#include "string.h"
#include "iot_debug.h"
#include "iot_lcd.h"
#include "iot_pmd.h"
#include "demo_camera.h"
#include "demo_lcd_9341.h"
#include "demo_zbar.h"

APP_STATUS g_app_status = APP_STATUS_IDLE;

extern void keypad_init(void);
extern void disp_init(void);

VOID app_main(VOID)
{
  iot_os_sys_request_freq(OPENAT_SYS_FREQ_312M);
  iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
  iot_pmd_exit_deepsleep();

  // lcd 初始化
  lcd_open();

  // lcd 显示
  disp_init();

  // 摄像头初始化 注:摄像头的初始化要放到lcd的后面 
  camera_open();  

  // 扫码初始化
  zbar_task();

  // 按键初始化
  keypad_init();
}
