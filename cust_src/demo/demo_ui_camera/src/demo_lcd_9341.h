#ifndef __DEMO_LCD_CONFIG_H__
#define __DEMO_LCD_CONFIG_H__

#define LCD_9341

#define LCD_I2C_ADDR (0X21)
#define LCD_PREVIEW_END_X (320)
#define LCD_PREVIEW_END_Y (240)
#define CAMERA_PREVIEW_WITHE (640)
#define CAMERA_PREVIEW_HEIGHT (480)
extern void lcd_open(void);

void lcdSetWindowAddress(T_AMOPENAT_LCD_RECT_T *pRect);
#endif
