#include "string.h"
#include "iot_debug.h"
#include "iot_audio.h"
#include "iot_types.h"
#include "iot_fs.h"
#include "adpcm.h"
#define DBG(X, Y...)	iot_debug_print("%s %d:"X, __FUNCTION__, __LINE__, ##Y)

//带长度控制的循环缓冲结构
typedef struct
{
	uint8_t *Data;
	uint32_t Len;
	uint32_t Offset;
	uint32_t MaxLength;
	uint32_t DataSize;
}RBuffer;

uint32_t WriteRBufferForce(RBuffer *Buf, void *Src, uint32_t Len)
{
	uint32_t i, p, cut_off = 0;
	uint8_t *Data = (uint8_t *)Src;
	cut_off = Buf->MaxLength - Buf->Len;
	if (cut_off >= Len)
	{
		cut_off = 0;
	}
	else
	{
		cut_off = Len - cut_off;
	}

	if (Buf->DataSize > 1)
	{
		for (i = 0, p = Buf->Offset + Buf->Len; i < Len; i++, p++)
		{
			if (p >= Buf->MaxLength)
			{
				p -= Buf->MaxLength;
			}
			memcpy(Buf->Data + (p * Buf->DataSize), Data + (i * Buf->DataSize), Buf->DataSize);
		}
	}
	else
	{
		for (i = 0, p = Buf->Offset + Buf->Len; i < Len; i++, p++)
		{
			if (p >= Buf->MaxLength)
			{
				p -= Buf->MaxLength;
			}

			Buf->Data[p] = Data[i];
		}
	}


	Buf->Offset += cut_off;
	if (Buf->Offset >= Buf->MaxLength)
		Buf->Offset -= Buf->MaxLength;

	Buf->Len += Len;
	if (Buf->Len > Buf->MaxLength)
		Buf->Len = Buf->MaxLength;

	return Len;

}

uint32_t QueryRBuffer(RBuffer *Buf, void *Src, uint32_t Len)
{
	uint32_t i, p;
	uint8_t *Data = (uint8_t *)Src;
	if (Buf->Len < Len)
	{
		Len = Buf->Len;
	}
	if (Buf->DataSize > 1)
	{
		for (i = 0, p = Buf->Offset; i < Len; i++, p++)
		{
			if (p >= Buf->MaxLength)
			{
				p -= Buf->MaxLength;
			}
			memcpy(Data + (i * Buf->DataSize), Buf->Data + (p * Buf->DataSize), Buf->DataSize);
		}
	}
	else
	{
		for (i = 0, p = Buf->Offset; i < Len; i++, p++)
		{
			if (p >= Buf->MaxLength)
			{
				p -= Buf->MaxLength;
			}
			Data[i] = Buf->Data[p];
		}
	}

	//for query, don't update r_buffer control struct.
	//buf->len -= len;
	//buf->offset = p;
	return Len;
}

uint32_t ReadRBuffer(RBuffer *Buf, void *Src, uint32_t Len)
{
	uint32_t l;
	uint8_t *Data = (uint8_t *)Src;
	l = QueryRBuffer(Buf, Data, Len);
	Buf->Len -= l;
	Buf->Offset += l;
	if (Buf->Offset > Buf->MaxLength)
	{
		Buf->Offset -= Buf->MaxLength;

	}
	return l;
}

typedef enum
{
    OPENAT_PCM_STATUS_REQUEST_DATA,
    OPENAT_PCM_STATUS_NO_MORE_DATA,
    OPENAT_PCM_STATUS_END,
    OPENAT_PCM_STATUS_ERR,
    OPENAT_PCM_STATUS_QTY
} OPENAT_PCM_PLAY_STATUS_T;

typedef uint32_t (*AUDIO_PCM_CALLBACK)(uint8_t Event, void *Buf, uint32_t Len);

typedef struct
{
	uint32_t *pBuffer;
    uint32_t Len;
    AUDIO_PCM_CALLBACK Handler;
    uint32_t sample_rate;
    uint32_t bit_rate;
    uint8_t isRecord;
    uint8_t isClose;
}T_AMOPENAT_AUDIO_PCM_PARAM;

/** 启动PCM格式录音，必须在任务中使用
*@param  pBuffer:   录音数据缓存
*@param  BufferLen	录音数据缓存区长度
*@param  Handler	录音数据回调处理
*@return	TRUE: 	    成功
*           FALSE:      失败
**/
BOOL iot_pcm_record_start(uint8_t *pBuffer, uint32_t BufferLen, AUDIO_PCM_CALLBACK Handler)
{
	T_AMOPENAT_AUDIO_PCM_PARAM Param;
	Param.Handler = Handler;
	Param.Len = BufferLen;
	Param.pBuffer = pBuffer;
	Param.isRecord = 1;
	Param.isClose = 0;
	return IVTBL(audioRec_start)(OPENAT_REC_MODE_PCM, 1, (AUDIO_REC_CALLBACK)&Param);
}

/** 停止PCM格式录音，必须在任务中使用
*@param  NONE
*@return	TRUE: 	    成功
*           FALSE:      失败
**/
BOOL iot_pcm_record_stop(void)
{
	T_AMOPENAT_AUDIO_PCM_PARAM Param;
	Param.isRecord = 1;
	Param.isClose = 1;
	return IVTBL(audioRec_start)(OPENAT_REC_MODE_PCM, 1, (AUDIO_REC_CALLBACK)&Param);
}

/** 启动PCM格式放音，必须在任务中使用
*@param  pBuffer:   放音数据缓存
*@param  BufferLen	放音数据缓存区长度，如果实际放音长度比缓存区小，直接写入实际长度
*@param  Handler	放音数据回调处理
*@return	TRUE: 	    成功
*           FALSE:      失败
**/
BOOL iot_pcm_play_start(uint8_t *pBuffer, uint32_t BufferLen, uint32_t SampleRate, uint32_t BitRate, AUDIO_PCM_CALLBACK Handler)
{
	T_AMOPENAT_AUDIO_PCM_PARAM Param;
	Param.Handler = Handler;
	Param.Len = BufferLen;
	Param.pBuffer = pBuffer;
	Param.isRecord = 0;
	Param.isClose = 0;
	Param.sample_rate = SampleRate;
	Param.bit_rate = BitRate;
	return IVTBL(audioRec_start)(OPENAT_REC_MODE_PCM, 1, (AUDIO_REC_CALLBACK)&Param);
}

/** 停止PCM格式放音，必须在任务中使用
*@param  NONE
*@return	TRUE: 	    成功
*           FALSE:      失败
**/
BOOL iot_pcm_play_stop(void)
{
	T_AMOPENAT_AUDIO_PCM_PARAM Param;
	Param.isRecord = 0;
	Param.isClose = 1;
	return IVTBL(audioRec_start)(OPENAT_REC_MODE_PCM, 1, (AUDIO_REC_CALLBACK)&Param);
}

INT32 adpcm_writeWaveHeader(char *FilePath, uint8_t NeedClear)
{
    INT32 result=-1;

    UINT32 tmp;
    WAVE_HEADER_EX waveFormatHeader_ex;

    WAVE_DATA_HEADER waveDataHeader;

    INT32 fhd=iot_fs_open_file(FilePath, O_RDWR);
    UINT32  fileSize;
    //DEMO测试时使用同一个文件，因此需要在首次使用时清除内容
    if (NeedClear)
    {
    	if (fhd >= 0)
    	{
    		iot_fs_close_file(fhd);
    		iot_fs_delete_file(FilePath);
    	}
    	fhd = -1;
    }

    if (fhd < 0)
    {
    	fhd = iot_fs_create_file(FilePath);
    }
    if (fhd < 0)
    {
    	DBG("res=%d", fhd);
    	return fhd;
    }

    fileSize=iot_fs_seek_file(fhd,0,SEEK_END);
    result = iot_fs_seek_file(fhd,0,SEEK_SET);

    if(result)
    {
    	iot_fs_close_file(fhd);
        DBG("res=%d", result);
        return result;
    }

    //wav header

    memcpy(waveFormatHeader_ex.szRiff,"RIFF",4);
    waveFormatHeader_ex.dwFileSize = fileSize- 8;
    memcpy(waveFormatHeader_ex.szWaveFmt,"WAVEfmt ",8);
    waveFormatHeader_ex.dwFmtSize = 20;
    waveFormatHeader_ex.wFormatTag = 0x11;//IMA ADPCM
    waveFormatHeader_ex.nChannels = 1;
    waveFormatHeader_ex.nSamplesPerSec = 8000;
    waveFormatHeader_ex.wBitsPerSample = 4;
    waveFormatHeader_ex.nAvgBytesPerSec = 8000*256/505;//mono
    waveFormatHeader_ex.nBlockAlign = 256;
    waveFormatHeader_ex.cbSize = 2;
    waveFormatHeader_ex.samplesPerBlock = 505;
    memcpy(waveFormatHeader_ex.fact,"fact",4);
    waveFormatHeader_ex.factsize_low = 4;
    waveFormatHeader_ex.factsize_hi = 0;
    tmp= fileSize -sizeof(WAVE_HEADER_EX) -sizeof(WAVE_DATA_HEADER);
    tmp= (tmp*505/256) + (tmp%256)*2;
    waveFormatHeader_ex.sample_low = tmp&0xffff;
    waveFormatHeader_ex.sample_hi = (tmp>>16)&0xff;

    //write wav header
    result=iot_fs_write_file(fhd,(UINT8 *)&waveFormatHeader_ex,sizeof(WAVE_HEADER_EX));

    if(result!=sizeof(WAVE_HEADER_EX))
    {
    	DBG("res=%d", result);
        iot_fs_close_file(fhd);
        return -1;
    }
    //wav data header
    memcpy(waveDataHeader.szData,"data",4);
    waveDataHeader.dwDataSize = fileSize - 60;



    //write wav data header
    result=iot_fs_write_file(fhd,(UINT8 *)&waveDataHeader,sizeof(WAVE_DATA_HEADER));
    iot_fs_close_file(fhd);

    if(result!=sizeof(WAVE_DATA_HEADER))
    {
    	DBG("res=%d", result);
        return -1;
    }

    return 0;
}

#define DEMO_UART	OPENAT_UART_1
#define PCM_FRAME_LEN	(320)	//1帧PCM数据320byte,20ms
#define ADPCM_BLOCK_LEN	(ADPCM_BLOCK_SAMPLES * 2)	//1个ADPCM BLOCK解码出来有1010字节，505个点
#define TEST_PCM_CNT	(60)	//测试12s
static uint8_t PCMBuffer[PCM_FRAME_LEN * 20]; //PCM录音缓存，有400ms空间，每次回调正好200ms,
//static uint8_t PCMSaveBuffer[PCM_FRAME_LEN * 300];
static uint8_t PCMPlayBuffer[ADPCM_BLOCK_LEN * 30];//PCM放音缓存，60个ADPCM BLOCK，太小的话，容易有杂音
static uint8_t PCMFileDataBuf[4096+ADPCM_BLOCK_SIZE];//缓存ADPCM数据，达到4K后写入文件系统
static uint32_t PCMFileDataLen;//缓存ADPCM数据长度
static uint8_t PCMRecordDone = 0;
static uint8_t PCMPlayDone = 0;
static uint8_t PCMRecordSaveFisrt = 1;
static uint32_t PCMSaveCnt = 0;//记录PCM录音回调次数，5次1S
static uint32_t PCMNeedPlayLen = 0;
static uint32_t PCMSaveLen = 0;
static HANDLE hPCMTask;
static INT32 hPCMFileHandle;
static const PSTR RecordFilePath = "test_rec.wav";
static struct adpcm_context adpcm_cntx;
static uint8_t pbuf_adpcm[ADPCM_BLOCK_SIZE];//256 bytes
uint8_t UartDataBuf[ADPCM_BLOCK_LEN * 60 + ADPCM_BLOCK_SIZE * 2];
uint8_t UartTxBuf[1024];
RBuffer UartRBuf;
uint8_t PCMBlockData[ADPCM_BLOCK_LEN];//ADPCM解码缓存数据
RBuffer PCMBlockRBuf;
uint8_t UartTxBusy;
typedef struct {
    UINT8 Type;
    UINT8 Data;
}USER_MESSAGE;

enum
{
	USER_MESSAGE_PCM_RECORD_DONE,
	USER_MESSAGE_PCM_RECORD_SAVE,
	USER_MESSAGE_UART_TX_DONE,

};

uint32_t demo_pcm_record_callback(uint8_t Event, void *Buf, uint32_t Len)
{
	USER_MESSAGE * Msg;
    INT32  ret = -1;
    INT32 filesize;
    INT32 fhd;
    uint8_t *inData;
    uint32_t blk_size=0;
	uint32_t inbufcnt=Len/2;//sample count
	inData = Buf;
	if (PCMRecordDone)
	{
		Msg = iot_os_malloc(sizeof(USER_MESSAGE));
		Msg->Type = USER_MESSAGE_PCM_RECORD_DONE;
		iot_os_send_message(hPCMTask, (PVOID)Msg);
		return 0;
	}

	switch (Event)
	{
	case OPENAT_STATUS_MID_BUFFER_REACHED:
	case OPENAT_STATUS_END_BUFFER_REACHED:
//		memcpy(PCMSaveBuffer + PCMSaveLen, Buf, Len);
//		PCMSaveLen += Len;
		while(inbufcnt>0){
			blk_size = 0;
			if(adpcm_cntx.block_samples==0)
			{
				adpcm_encode_block_header(&adpcm_cntx, pbuf_adpcm,(uint32_t*)&blk_size, (int16_t*)(inData +(Len -inbufcnt*2)),(uint32_t*)&inbufcnt);
			}
			//DBG("%d,%d,%d",adpcm_cntx.block_samples, blk_size, inbufcnt);
			if(inbufcnt>0)
			{
				adpcm_encode_chunks (&adpcm_cntx, pbuf_adpcm+blk_size,
						(uint32_t*)&blk_size,(int16_t*)(inData+(Len -inbufcnt*2)), (uint32_t*)&inbufcnt);
			}
			if(adpcm_cntx.block_samples==505)
				adpcm_cntx.block_samples= 0;

			memcpy(PCMFileDataBuf + PCMFileDataLen, pbuf_adpcm, blk_size);
			PCMFileDataLen += blk_size;
			WriteRBufferForce(&UartRBuf, pbuf_adpcm, blk_size);//从串口输出编码过的数据


			if(PCMFileDataLen>=4096)
			{
//				Msg = iot_os_malloc(sizeof(USER_MESSAGE));
//				Msg->Type = USER_MESSAGE_PCM_RECORD_SAVE;
//				iot_os_send_message(hPCMTask, (PVOID)Msg);
				DBG("%d", PCMFileDataLen);
				if (PCMRecordSaveFisrt)
				{
					if (adpcm_writeWaveHeader(RecordFilePath, 1) < 0)
					{
						DBG("record file error!");
						PCMRecordDone = 1;
						iot_pcm_record_stop();
						iot_audio_open_speaker();
						break;
					}
					PCMRecordSaveFisrt = 0;
					DBG("record file head first done");
				}
			    fhd=iot_fs_open_file(RecordFilePath, O_RDWR);
			    iot_fs_seek_file(fhd,0,SEEK_END);
				iot_fs_write_file(fhd, PCMFileDataBuf, PCMFileDataLen);
				PCMFileDataLen = 0;
				iot_fs_close_file(fhd);
			}

		}
		if (!UartTxBusy)
		{
			Msg = iot_os_malloc(sizeof(USER_MESSAGE));
			Msg->Type = USER_MESSAGE_UART_TX_DONE;
			iot_os_send_message(hPCMTask, (PVOID)Msg);
		}

		PCMSaveCnt++;
		if (PCMSaveCnt >= TEST_PCM_CNT)
		{
			DBG("buffer full, stop record, try to play");
			Msg = iot_os_malloc(sizeof(USER_MESSAGE));
			Msg->Type = USER_MESSAGE_PCM_RECORD_DONE;
			iot_os_send_message(hPCMTask, (PVOID)Msg);

		}
		break;
	case OPENAT_STATUS_NO_MORE_DATA:
	case OPENAT_STATUS_ERR:
		Msg = iot_os_malloc(sizeof(USER_MESSAGE));
		Msg->Type = USER_MESSAGE_PCM_RECORD_DONE;
		iot_os_send_message(hPCMTask, (PVOID)Msg);
		break;
	}
	return 0;
}

uint32_t demo_pcm_play_callback(uint8_t Event, void *Buf, uint32_t Len)
{
	USER_MESSAGE * Msg;
	uint32_t DummyLen = 0;
	uint8_t TempData[ADPCM_BLOCK_LEN];
	INT32 ReadLen;
	uint32_t RestLen = Len;
	uint32_t DataLen;
	struct adpcm_state AdpcmState;
	char *outData = (char *)Buf;
	char *inData = (char *)pbuf_adpcm;
	if (PCMPlayDone)
	{
		iot_audio_close_speaker();
		return 0;
	}

	switch (Event)
	{
	case OPENAT_PCM_STATUS_REQUEST_DATA:
	case OPENAT_PCM_STATUS_NO_MORE_DATA:
		while (RestLen)
		{
			//先发送完解码缓存区的数据
			if (PCMBlockRBuf.Len >= RestLen)
			{
				DataLen = ReadRBuffer(&PCMBlockRBuf, TempData, RestLen);
				memcpy(outData + DummyLen, TempData, DataLen);
				DummyLen += DataLen;
				RestLen = 0;
				break;
			}
			else
			{
				DataLen = ReadRBuffer(&PCMBlockRBuf, TempData, PCMBlockRBuf.Len);
				memcpy(outData + DummyLen, TempData, DataLen);
				DummyLen += DataLen;
				RestLen -= DataLen;
			}
			PCMBlockRBuf.Len = 0;
			PCMBlockRBuf.Offset = 0;
			if (PCMPlayDone)
			{
				break;
			}
			//发送完解码缓存区的数据后，从文件里读出内容，解码写入缓存
			ReadLen = iot_fs_read_file(hPCMFileHandle, inData, ADPCM_BLOCK_SIZE);
			if (ReadLen != ADPCM_BLOCK_SIZE)
			{
				DBG("play done %d", ReadLen);
				PCMPlayDone = 1;
				iot_fs_close_file(hPCMFileHandle);
			}
			if (ReadLen)
			{
				AdpcmState.index = inData[2];
				memcpy(&AdpcmState.valprev, inData, 2);
				memcpy(PCMBlockData, inData, 2);
				adpcm_decoder(&inData[4],PCMBlockData + 2,ReadLen-4,&AdpcmState);
				
				PCMBlockRBuf.Len = (ReadLen-4)*4+2;
				WriteRBufferForce(&UartRBuf, PCMBlockData, PCMBlockRBuf.Len);//从串口输出解码过的数据
			}
		}

		DBG("%d", DummyLen);

		if (!UartTxBusy)
		{
			Msg = iot_os_malloc(sizeof(USER_MESSAGE));
			Msg->Type = USER_MESSAGE_UART_TX_DONE;
			iot_os_send_message(hPCMTask, (PVOID)Msg);
		}
		return DummyLen;	//有数据写入时，必须返回写入长度
	default:
		break;
	}
	return 0;
}

//中断方式读串口1数据
//注: 中断中有复杂的逻辑,要发送消息到task中处理
void uart_recv_handle(T_AMOPENAT_UART_MESSAGE* evt)
{
    INT8 recv_buff[64] = {0};
    int32 recv_len;
    int32 dataLen = evt->param.dataLen;
    USER_MESSAGE * Msg;
    switch(evt->evtId)
    {
        case OPENAT_DRV_EVT_UART_RX_DATA_IND:
            recv_len = iot_uart_read(DEMO_UART, recv_buff, dataLen , 10);
            break;
        case OPENAT_DRV_EVT_UART_TX_DONE_IND:
			Msg = iot_os_malloc(sizeof(USER_MESSAGE));
			Msg->Type = USER_MESSAGE_UART_TX_DONE;
			iot_os_send_message(hPCMTask, (PVOID)Msg);
			UartTxBusy = 0;
            break;
        default:
            break;
    }
}

static void demo_pcm_start_decode_and_play(void)
{
	INT32 ReadLen;
	struct adpcm_state AdpcmState;
	char *outData = (char *)PCMPlayBuffer;
	char *inData = (char *)pbuf_adpcm;
    uint32_t DummyLen = 0;
    uint32_t Len = sizeof(PCMPlayBuffer);
    uint8_t Fisrt = 1;
#if 1
	iot_fs_seek_file(hPCMFileHandle, SIZE_WAVE_HEADER_EX + SIZE_WAVE_DATA_HEADER, SEEK_SET);

	while ( ((Len - DummyLen) >= ADPCM_BLOCK_LEN) && !PCMPlayDone)
	{
		ReadLen = iot_fs_read_file(hPCMFileHandle, inData, ADPCM_BLOCK_SIZE);
		if (ReadLen != ADPCM_BLOCK_SIZE)
		{
			DBG("play done %d", ReadLen);
			PCMPlayDone = 1;
			iot_fs_close_file(hPCMFileHandle);

		}
		if (ReadLen)
		{
			if (Fisrt)
			{
				AdpcmState.index = 0;
				Fisrt = 0;
			}
			else
			{
				AdpcmState.index = inData[2];
			}

			memcpy(&AdpcmState.valprev, inData, 2);
			memcpy(outData + DummyLen, inData, 2);
			adpcm_decoder(&inData[4],&outData[DummyLen + 2],ReadLen-4,&AdpcmState);
			DummyLen += (ReadLen-4)*4+2;
		}

	}
	DBG("%d", DummyLen);
#endif
	iot_pcm_play_start(PCMPlayBuffer, sizeof(PCMPlayBuffer), 8000, 16, demo_pcm_play_callback);
	WriteRBufferForce(&UartRBuf,outData, DummyLen);//从串口输出解码过的数据
	if (!UartTxBusy)
	{
		DummyLen = ReadRBuffer(&UartRBuf, UartTxBuf, sizeof(UartTxBuf));
		if (DummyLen)
		{
			UartTxBusy = 1;
			iot_uart_write(DEMO_UART, UartTxBuf, DummyLen);
		}
	}
}

static void demo_pcm_task(PVOID pParameter)
{
	USER_MESSAGE*    msg;
	uint32_t TxLen;
	INT32 fhd;
	while (1)
	{
		iot_os_wait_message(hPCMTask, (PVOID)&msg);
        switch(msg->Type)
        {
		case USER_MESSAGE_PCM_RECORD_DONE:
			if (!PCMRecordDone)
			{
				PCMRecordDone = 1;
				iot_pcm_record_stop();
				iot_audio_open_speaker();
				if(PCMFileDataLen)
				{
					DBG("%d", PCMFileDataLen);
					if (PCMRecordSaveFisrt)
					{
						if (adpcm_writeWaveHeader(RecordFilePath, 1) < 0)
						{
							DBG("record file error!");
						}
						PCMRecordSaveFisrt = 0;
					}
					hPCMFileHandle=iot_fs_open_file(RecordFilePath, O_RDWR);
				    iot_fs_seek_file(hPCMFileHandle, 0, SEEK_END);
					iot_fs_write_file(hPCMFileHandle, PCMFileDataBuf, PCMFileDataLen);
					PCMFileDataLen = 0;
					iot_fs_close_file(hPCMFileHandle);

				}
				//重新写入文件头 ，此时写入了正确的数据长度
				adpcm_writeWaveHeader(RecordFilePath, 0);
				hPCMFileHandle = iot_fs_open_file(RecordFilePath, O_RDWR);
				DBG("%d", iot_fs_seek_file(hPCMFileHandle, 0, SEEK_END));
				//已经知道是ADPCM编码，跳过文件头，直接开始解码输出
				demo_pcm_start_decode_and_play();
			}
			break;
		case USER_MESSAGE_UART_TX_DONE:
			if (!UartTxBusy)
			{
				TxLen = ReadRBuffer(&UartRBuf, UartTxBuf, sizeof(UartTxBuf));
				if (TxLen)
				{
					UartTxBusy = 1;
					iot_uart_write(DEMO_UART, UartTxBuf, TxLen);
				}
			}
			break;
		case USER_MESSAGE_PCM_RECORD_SAVE:

			break;
		default:
			break;
        }
        iot_os_free(msg);
	}
}

void demo_pcm_init(void)
{
	iot_audio_set_channel(OPENAT_AUD_CHANNEL_LOUDSPEAKER);
	iot_audio_set_speaker_gain(OPENAT_AUD_SPK_GAIN_18dB);
	iot_audio_set_channel_with_same_mic(OPENAT_AUD_CHANNEL_HANDSET, OPENAT_AUD_CHANNEL_LOUDSPEAKER);
	iot_pcm_record_start(PCMBuffer, sizeof(PCMBuffer), demo_pcm_record_callback);
	adpcm_cntx.num_channels = 1;	//MIC是单声道
    BOOL err;
    T_AMOPENAT_UART_PARAM uartCfg;

    memset(&uartCfg, 0, sizeof(T_AMOPENAT_UART_PARAM));
    uartCfg.baud = OPENAT_UART_BAUD_460800; //波特率
    uartCfg.dataBits = 8;   //数据位
    uartCfg.stopBits = 1; // 停止位
    uartCfg.parity = OPENAT_UART_NO_PARITY; // 无校验
    uartCfg.flowControl = OPENAT_UART_FLOWCONTROL_NONE; //无流控
    uartCfg.txDoneReport = TRUE; // 设置TURE可以在回调函数中收到OPENAT_DRV_EVT_UART_TX_DONE_IND
    uartCfg.uartMsgHande = uart_recv_handle; //回调函数

    // 配置uart1 使用中断方式读数据
    err = iot_uart_config(DEMO_UART, &uartCfg);

    hPCMTask = iot_os_create_task(demo_pcm_task,
                        NULL,
                        20480,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "demo_pcm");
    UartRBuf.Data = UartDataBuf;
    UartRBuf.DataSize = 1;
    UartRBuf.Len = 0;
    UartRBuf.MaxLength = sizeof(UartDataBuf);
    UartRBuf.Offset = 0;
    PCMBlockRBuf.Data = PCMBlockData;
    PCMBlockRBuf.DataSize = 1;
    PCMBlockRBuf.Len = 0;
    PCMBlockRBuf.MaxLength = sizeof(PCMBlockData);
    PCMBlockRBuf.Offset = 0;
}
void app_main(void)
{
	//iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
	demo_pcm_init();
}
