#include "string.h"
#include "iot_debug.h"
#include "iot_sys.h"
#include "iot_uart.h"
#include "iot_pmd.h"
#include "iot_os.h"
#include "iot_network.h"
#include "iot_socket.h"
#include "NmeaParser.h"
#include "RxP.h"
#include "commUtil.h"

#define SOCKET_MSG_NETWORK_READY (0)
#define SOCKET_MSG_NETWORK_LINKED (1)
#define CMD_CGSN  "AT+WIMEI?\r\n"
#define socket_dbg iot_debug_print
#define GPS_PRINT iot_debug_print

#define DEMO_SERVER_TCP_IP "180.97.81.180"
#define DEMO_SERVER_TCP_PORT 56621
typedef struct {
    UINT8 type;
    UINT8 data;
}DEMO_SOCKET_MESSAGE;

static HANDLE g_s_vatTimer;
static HANDLE g_s_socket_task;


extern T_AMOPENAT_INTERFACE_VTBL* g_s_InterfaceVtbl;
extern void example_init();
extern void example_proc();

/*以下函数用于获取GPS关键数据*/
extern NMEA_RMC_T* gpsDataGetRmc(void);
extern NMEA_GGA_T* gpsDataGetGGA(void);

char buf1[64]={0};//存放IMEI号
char buf2[64]={0};//存放GPS信息(经纬度)

static int demo_socket_tcp_recv(int socketfd)
{
    unsigned char recv_buff[64] = {0};
    int recv_len;

    // TCP 接受数据
    recv_len = recv(socketfd, recv_buff, sizeof(recv_buff), 0);
    socket_dbg("[yzs] tcp recv result %d data %s", recv_len, recv_buff);
   
    return recv_len;
}

static int demo_socket_tcp_send(int socketfd)
{
    int send_len;

	char data[64] = {0};

	memset(data, 0, sizeof(data));
    strcat(buf1,buf2);
   
    memcpy(data,buf1,sizeof(buf1));
    
    //如果获取到GPS信息
    if(strlen(buf2)!=0)
    {
    // TCP 发送数据
    iot_os_sleep(3000);
    send_len = send(socketfd, data, sizeof(data), 0);
    socket_dbg("[socket] tcp send datd result = %d", send_len);
    memset(buf1+21,0,sizeof(buf2));
    return send_len;
    }
}


static int demo_socket_tcp_connect_server(void)
{
    int socketfd;
    int connErr;
    struct sockaddr_in tcp_server_addr; 
    
    // 创建tcp socket
    socketfd = socket(AF_INET,SOCK_STREAM,0);
    if (socketfd < 0)
    {
        socket_dbg("[socket] create tcp socket error");
        return -1;
    }
       
    socket_dbg("[socket] create tcp socket success");
    
    // 建立TCP链接
    memset(&tcp_server_addr, 0, sizeof(tcp_server_addr)); // 初始化服务器地址  
    tcp_server_addr.sin_family = AF_INET;  
    tcp_server_addr.sin_port = htons((unsigned short)DEMO_SERVER_TCP_PORT);  
    inet_aton(DEMO_SERVER_TCP_IP,&tcp_server_addr.sin_addr);

    socket_dbg("[socket] tcp connect to addr %s", DEMO_SERVER_TCP_IP);
    connErr = connect(socketfd, (const struct sockaddr *)&tcp_server_addr, sizeof(struct sockaddr));
    

    if (connErr < 0)
    {
        socket_dbg("[socket] tcp connect error %d", socket_errno(socketfd));
        close(socketfd);
        return -1;
    }
    socket_dbg("[socket] tcp connect success");

    return socketfd;
}

static void demo_socket_tcp_client()
{
    int socketfd, ret, err;
    struct timeval tm;
    fd_set readset;
    int optLen = 4, sendBufSize;

    tm.tv_sec = 2;
    tm.tv_usec = 0;

    socketfd = demo_socket_tcp_connect_server();

    if (socketfd >= 0)
    {
        ret = getsockopt(socketfd, SOL_SOCKET, SO_SNDWNDBUF, (void*)&sendBufSize, (socklen_t*)&optLen);

        if(ret < 0)
        {
            socket_dbg("[socket] getsockopt error = %d", ret);
        }
        else
        {
            socket_dbg("[socket] getsockopt original SO_SNDBUF = %d", sendBufSize);
        }
        while(1)
        {
            ret = demo_socket_tcp_send(socketfd);
            if(ret < 0)
            {
            	err = socket_errno(socketfd);
				//socket_dbg("[socket] send last error %d", err);
            	if(err != EWOULDBLOCK)
            	{
                	//break;
            	}
				else
				{
					iot_os_sleep(10);
					continue;
				}
            }

            ret = getsockopt(socketfd, SOL_SOCKET, SO_SNDWNDBUF, (void*)&sendBufSize, (socklen_t*)&optLen);

            if(ret < 0)
            {
                socket_dbg("[socket] getsockopt error = %d", ret);
            }
            else
            {
                socket_dbg("[socket] getsockopt after send SO_SNDBUF = %d", sendBufSize);
            }
            FD_ZERO(&readset);
            FD_SET(socketfd, &readset);
            ret = select(socketfd+1, &readset, NULL, NULL, &tm);
            if(ret > 0)
            {
    			ret = demo_socket_tcp_recv(socketfd);
                if(ret == 0)
                {
                    socket_dbg("[socket] recv close");
                    iot_os_sleep(1000);
                }
                else if(ret < 0)
                {
                    socket_dbg("[socket] recv error %d", socket_errno(socketfd));
                    iot_os_sleep(1000);
                }
            }
            else if(ret == 0)
            {
                socket_dbg("[socket] select timeout");
            }

            ret = getsockopt(socketfd, SOL_SOCKET, SO_SNDWNDBUF, (void*)&sendBufSize, (socklen_t*)&optLen);

            if(ret < 0)
            {
                socket_dbg("[socket] getsockopt error = %d", ret);
            }
            else
            {
                socket_dbg("[socket] getsockopt after recv SO_SNDBUF = %d", sendBufSize);
            }
        }
        close(socketfd);
    }
}

static void demo_network_connetck(void)
{
    T_OPENAT_NETWORK_CONNECT networkparam;
    
    memset(&networkparam, 0, sizeof(T_OPENAT_NETWORK_CONNECT));
    memcpy(networkparam.apn, "CMNET", strlen("CMNET"));

    iot_network_connect(&networkparam);

}

static void demo_networkIndCallBack(E_OPENAT_NETWORK_STATE state)
{
    DEMO_SOCKET_MESSAGE* msgptr = iot_os_malloc(sizeof(DEMO_SOCKET_MESSAGE));
    socket_dbg("[socket] network ind state %d", state);
    if(state == OPENAT_NETWORK_LINKED)
    {
        msgptr->type = SOCKET_MSG_NETWORK_LINKED;
        iot_os_send_message(g_s_socket_task, (PVOID)msgptr);
        return;
    }
    else if(state == OPENAT_NETWORK_READY)
    {
        msgptr->type = SOCKET_MSG_NETWORK_READY;
        iot_os_send_message(g_s_socket_task,(PVOID)msgptr);
        return;
    }
    iot_os_free(msgptr);
}

static void demo_socket_task(PVOID pParameter)
{
    DEMO_SOCKET_MESSAGE*    msg;
    socket_dbg("[socket] wait network ready....");
    BOOL sock = FALSE;

    while(1)
    {
        iot_os_wait_message(g_s_socket_task, (PVOID)&msg);

        switch(msg->type)
        {
            case SOCKET_MSG_NETWORK_READY:
                socket_dbg("[socket] network connecting....");
                demo_network_connetck();
                break;
            case SOCKET_MSG_NETWORK_LINKED:
                socket_dbg("[socket] network connected");
                if(!sock)
                {
				//demo_gethostbyname(); 
				//demo_socket_udp_client();
     			demo_socket_tcp_client();
                sock = TRUE;
                }
                break;
        }

        iot_os_free(msg);
    }
}

void demo_socket_init(void)
{ 
    socket_dbg("[socket] demo_socket_init");

    //注册网络状态回调函数
    iot_network_set_cb(demo_networkIndCallBack);

    g_s_socket_task = iot_os_create_task(demo_socket_task,
                        NULL,
                        4096,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "demo_socket");
}

static void rxDataInd(T_AMOPENAT_UART_MESSAGE* evt)
{
  char data[1024];
  int len;
  if(evt->evtId == OPENAT_DRV_EVT_UART_RX_DATA_IND)
  {
    len = iot_uart_read(OPENAT_UART_2, data, evt->param.dataLen, 0);

    if(len > 0)
    {
      int i;
      for(i = 0; i < len; i++)
      { 
        rxp_pcrx_nmea(data[i]);
      }
    }
  }
}

static BOOL gpsIsFixed()
{

  NMEA_RMC_T* rmc = gpsDataGetRmc();
 
  
  if(rmc->cStatus == 'A')
  {
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

static void demo_gps_task(PVOID pParameter)
{
  T_AMOPENAT_UART_PARAM uartCfg;

  UINT16 staUsed, staView;

  char latStr[32];
  char lonStr[32];

  NMEA_GGA_T* ggaPtr;

  memset(&uartCfg, 0, sizeof(T_AMOPENAT_UART_PARAM));
  uartCfg.baud = OPENAT_UART_BAUD_115200;
  uartCfg.dataBits = 8;
  uartCfg.stopBits = 1;
  uartCfg.parity = OPENAT_UART_NO_PARITY; 
  uartCfg.flowControl = OPENAT_UART_FLOWCONTROL_NONE; 
  uartCfg.txDoneReport = FALSE;
  uartCfg.uartMsgHande = rxDataInd;

  iot_uart_config(OPENAT_UART_2, &uartCfg);


  example_init();
    

  while(1)
  {
    staUsed = 0;
    staView = 0;
    example_proc();

    iot_os_sleep(1000);

    /*输出GPS数据*/

    ggaPtr = gpsDataGetGGA();

    if(gpsIsFixed())
    {
      commUtilFormatFloat(latStr, 32, ggaPtr->dfLatitude, 6);
      commUtilFormatFloat(lonStr, 32, ggaPtr->dfLongitude, 6);
      GPS_PRINT("[gps] is fixed [LOC = (%s,%s)]", 
              latStr, lonStr);
      sprintf(buf2,"LOC=[%s,%s]",latStr, lonStr);
    }
    else
    {
      GPS_PRINT("[gps] is not fix");
    }
  }
  
}

VOID vatAtIndHandle(UINT8 *pData, UINT16 length)
{
	iot_debug_print("[AT] AT RECV: %s", pData);
    if(strstr(pData,"+WIMEI:")!=NULL)
    {
        sprintf(buf1,"%s",pData+4);
    }
}

VOID vatTimerhandle(T_AMOPENAT_TIMER_PARAMETER *pParameter)
{   
    //发送查询IMEI号
    iot_os_sleep(2000);
    iot_debug_print("[AT] send %s", CMD_CGSN);
    iot_vat_sendATcmd(CMD_CGSN, strlen(CMD_CGSN));
}

static void demo_timer_task(PVOID pParamter)
{
    iot_vat_init(vatAtIndHandle);
    g_s_vatTimer = iot_os_create_timer(vatTimerhandle, NULL);
	
	iot_os_start_timer(g_s_vatTimer, 2000);
}

VOID app_main(VOID)
{
    /*1, 打开GPS供电和32K时钟*/
    iot_pmd_poweron_ldo(OPENAT_LDO_POWER_CAM, 7);
    IVTBL(sys32k_clk_out)(1);
    
    /*2, 新建TASK解析GPS串口数据*/
    iot_os_create_task(demo_gps_task,
                        NULL,
                        40960,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "demo_gps");
    iot_os_create_task(demo_timer_task,
                        NULL,
                        40960,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "demo_timer");
    iot_debug_set_fault_mode(OPENAT_FAULT_HANG);
    demo_socket_init();
}


