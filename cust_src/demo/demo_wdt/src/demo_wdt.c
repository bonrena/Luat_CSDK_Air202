#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"
#include "iot_gpio.h"

typedef enum
{
  WDT_FEED_IDLE,
  WDT_FEED_DO,
  WDT_FEED_CHECK,
  WDT_FEED_FAIL,
  WDT_FEED_SUCCESS,
}WDT_FEED_STATUS;

WDT_FEED_STATUS g_wdt_feed_status = WDT_FEED_IDLE;
HANDLE wdtTimer;

#define WDPIN_WDI OPENAT_GPIO_31
#define WDPIN_RESET_IN OPENAT_GPIO_30
#define WDT_ENABLE (1)
#define WDT_DISABLE (0)
#define CHECK_SUCCESS (0)
#define CHECK_FAIL (1)
#define T_FEED (120 * 1000) //ms
#define T_DELAY (2000) // ms
#define T_CHECK (100) //ms
#define T_CHECK_MAX_COUNT (T_DELAY/T_CHECK) 
#define T_BBLOW (2000) //ms
#define T_RESTIN (100) //ms


// 喂狗定时器
void wdt_timerHanle(T_AMOPENAT_TIMER_PARAMETER *pParameter)
{
  T_AMOPENAT_GPIO_CFG cfg;

  iot_debug_print("[wdt] wdt_timerHanle g_wdt_feed_status %d", g_wdt_feed_status);

  switch(g_wdt_feed_status)
  {
    // step1: WDPIN_WDI 配置成输出, 拉低2秒
    case WDT_FEED_IDLE:
    {
      iot_debug_print("[wdt] WDT->WDT_FEED_IDLE");
      memset(&cfg, 0, sizeof(cfg));
      iot_gpio_close(WDPIN_WDI);

      cfg.mode = OPENAT_GPIO_OUTPUT;
      cfg.param.defaultState =  TRUE;

      // 配置输出
      iot_gpio_config(WDPIN_WDI, &cfg);

      // 拉低
      iot_gpio_set(WDPIN_WDI, 0);

      g_wdt_feed_status = WDT_FEED_DO;
      iot_os_start_timer(wdtTimer, T_BBLOW);
      break;
    }
    // step2: WDPIN_WDI 配置成输入, 检测喂狗是否成功
    case WDT_FEED_DO:
    {
      iot_debug_print("[wdt] WDT->WDT_FEED_DO");
      // 配置成输入
      iot_gpio_close(WDPIN_WDI);
      
      memset(&cfg, 0, sizeof(cfg));
      cfg.mode = OPENAT_GPIO_INPUT;
      iot_gpio_config(WDPIN_WDI, &cfg);

      // 定时器检测 
      g_wdt_feed_status = WDT_FEED_CHECK;
      iot_os_start_timer(wdtTimer, T_CHECK);
      break;
    }

    // step3: 喂狗失败重启单片机,成功设置下次喂狗定时器120s 
    case WDT_FEED_CHECK:
    {
      static UINT8 checkCount = 0;
      UINT8 check = 0;

      iot_debug_print("[wdt] WDT->WDT_FEED_CHECK");
      iot_gpio_read(WDPIN_WDI, &check);

      iot_debug_print("[wdt] wdt_timerHanle WDT_FEED_CHECK checkCount %d, %d", checkCount, check);
      // 检测超过最大次数重启看门狗
      if (checkCount > T_CHECK_MAX_COUNT)
      {
        checkCount = 0;
        iot_gpio_set(WDPIN_RESET_IN, WDT_DISABLE);
        g_wdt_feed_status = WDT_FEED_FAIL;
        iot_os_start_timer(wdtTimer, T_RESTIN);
      }
      // 检测失败继续检测
      else if (check == CHECK_FAIL)
      {
        checkCount++;
        g_wdt_feed_status = WDT_FEED_CHECK;
        iot_os_start_timer(wdtTimer, T_CHECK);
      }
      // 检测成功, 设置下次喂狗定时器
      else if (check == CHECK_SUCCESS)
      {
        checkCount = 0;
        g_wdt_feed_status = WDT_FEED_SUCCESS;
        iot_os_start_timer(wdtTimer, T_CHECK);
      }
      break;
    }
    // step4 :重启单片机
    case WDT_FEED_FAIL:
    {
      iot_debug_print("[wdt] WDT->WDT_FEED_FAIL");
      iot_gpio_set(WDPIN_RESET_IN, WDT_ENABLE);
      // 开启120定时器喂狗
      g_wdt_feed_status = WDT_FEED_IDLE;
      iot_os_start_timer(wdtTimer, T_FEED);
      break;
    }
    // step5 :重新设置120定时器喂狗
    case WDT_FEED_SUCCESS:
    {
      iot_debug_print("[wdt] WDT->WDT_FEED_SUCCESS");
      // 开启120定时器喂狗
      g_wdt_feed_status = WDT_FEED_IDLE;
      iot_os_start_timer(wdtTimer, T_FEED);
      break;
    }
    default:
      iot_debug_assert(FALSE, __FUNCTION__, __LINE__);
  }
}

// 喂狗
void wdt_feed(void)
{
  g_wdt_feed_status = WDT_FEED_IDLE;
  iot_os_start_timer(wdtTimer, T_FEED);

  iot_debug_print("[wdt] wdt_timerHanle g_wdt_feed_status %d", g_wdt_feed_status);
}


// 打开看门狗
void wdt_open(void)
{
  T_AMOPENAT_GPIO_CFG  cfg;

  // 使能看门狗
  memset(&cfg, 0, sizeof(cfg));
  cfg.mode = OPENAT_GPIO_OUTPUT;
  cfg.param.defaultState = WDT_ENABLE;
  iot_gpio_config(WDPIN_RESET_IN, &cfg);

  // 创建定时器
  wdtTimer = iot_os_create_timer(wdt_timerHanle, NULL);

  iot_debug_print("[wdt] wdt_open g_wdt_feed_status %d", g_wdt_feed_status);
}

/*
      测试的板子是AIR800_M4_A12
      如果其他的板子需要修改WDPIN_WDI 和 WDPIN_RESET_IN的管脚定义
*/
void app_main(void)
{
    iot_os_sleep(2000);

    iot_debug_print("[wdt] app_main");

    // 打开开门狗
    wdt_open();

    // 喂狗, 如果不喂狗250秒左右会重启模块
    wdt_feed();
}
