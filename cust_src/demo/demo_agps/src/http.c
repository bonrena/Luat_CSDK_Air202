
#include "http.h"
#include "agps.h"
#define OS(X)	X
#define DBG(X, Y...)	iot_debug_print("%s %d:"X, __FUNCTION__, __LINE__, ##Y)

int32_t HTTP_ResponseCode( uint8 *Http_recevieBuf )
{
    int32_t response_code=0;
    int8_t* p_start = NULL;
    int8_t* p_end =NULL;
    int8_t re_code[10] ={0};
    OS(memset)(re_code,0,sizeof(re_code));

    p_start = (int8_t*)strchr((const char*)Http_recevieBuf, ' ' );
    if(NULL == p_start)
    {
    	DBG("!");
        return -1;
    }
    p_end = (int8_t*)strchr( (const char*)++p_start, ' ' );
    if(p_end)
    {
        if(p_end - p_start > sizeof(re_code))
        {
        	DBG("!");
            return -1;
        }
        OS(memcpy)( re_code,p_start,(p_end-p_start) );
    }
    else
    {
    	DBG("!");
    }
    DBG("%s", re_code);
    response_code = OS(strtoul)((char*)re_code, NULL, 10);
    return response_code;
}

/*********************************************************************
*
*   FUNCTION       :   TO get the http headlen
*     httpbuf      :   http receive buf
*     return       :   the http headlen.
*   Add by Alex lin  --2014-12-02
*
**********************************************************************/
int32_t HTTP_HeadLen( uint8 *httpbuf )
{
   int8_t *p_start = NULL;
   int8_t *p_end =NULL;
   int32_t headlen=0;
   p_start = (int8 *)httpbuf;
   p_end = (int8 *)OS(strstr)( (char *)httpbuf, "\r\n\r\n");
   if( p_end==NULL )
   {
       return -1;
   }
   p_end=p_end+OS(strlen)("\r\n\r\n");
   headlen = ((uint32_t)p_end-(uint32_t)p_start);
   return headlen;
}

/*********************************************************************
*
*   FUNCTION       :   TO get the http bodylen
*      httpbuf     :   http receive buf
*      return      :   the http bodylen.(0-error)
*   Add by Alex lin  --2014-12-02
*
**********************************************************************/
int32_t HTTP_BodyLen( uint8 *httpbuf )
{
   int8_t *p_start = NULL;
   int8_t *p_end =NULL;
   int8_t bodyLenbuf[10]={0};
   int32_t bodylen=0;  //Content-Length:
   p_start = (int8 *)OS(strstr)( (char *)httpbuf,"Content-Length: ");
   if( p_start==NULL ) return -1;
   p_start = p_start+OS(strlen)("Content-Length: ");
   p_end = (int8 *)OS(strstr)((char*)p_start,"\r\n");
   if( p_end==NULL )   return -1;

   OS(memcpy)( bodyLenbuf,p_start,(p_end-p_start));
   bodylen = OS(strtoul)(bodyLenbuf, NULL, 10);
   return bodylen;
}


