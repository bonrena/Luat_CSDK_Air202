#include "string.h"
#include "iot_debug.h"
#include "iot_uart.h"
#include "iot_pmd.h"
#include "iot_socket.h"
#include "NmeaParser.h"
#include "RxP.h"
#include "commUtil.h"
#include "agps.h"
#include "http.h"
#define DBG(X, Y...)	iot_debug_print("%s %d:"X, __FUNCTION__, __LINE__, ##Y)

#define dow(y,m,d) \
  ((((((m)+9)%12+1)<<4)%27 + (d) + 1 + \
  ((y)%400+400) + ((y)%400+400)/4 - ((y)%400+400)/100 + \
  (((m)<=2) ? ( \
  (((((y)%4)==0) && (((y)%100)!=0)) || (((y)%400)==0)) \
  ? 5 : 6) : 0)) % 7)

enum
{

	GOKE_AGPS_START,
	GOKE_AGPS_SENDING,
	GOKE_AGPS_SEND_DONE,
	GOKE_AGPS_END,
};


#define GOKE_CHANGE_BR_CMD	(149)
#define GPS_CMD_HEAD		"PGKC"
#define GOKE_GPD_START		"149,1,115200"
#define AGPS_HTTP_PORT		(7777)
#define SOCKET_CLOSE(A)         if (A >= 0) {close(A);A = -1;}

#define LUAT_LBS_PORT		(12411)
#define LUAT_LBS_REQ_LOCAT	(0x02)
static const char *LUAT_PRODUCTKEY = "v32xEAKsGTIEQxtqgwCldp5aPlcnPs3K";//正式使用时，请改成自己的
static const char *LBS_URL = "bs.openluat.com";

static const char *AGPS_HttpServer = "www.goke-agps.com";
static const char *AGPS_HttpGetString = "GET /brdcGPD.dat HTTP/1.1\r\nHost: www.goke-agps.com:7777\r\nConnection: Keep-Alive\r\n\r\n";

AGPS_CtrlStruct AGPSCtrl;
/*时间转换函数*/
struct tm tms;

struct tm
{
  int	tm_sec;
  int	tm_min;
  int	tm_hour;
  int	tm_mday;
  int	tm_mon;
  int	tm_year;
  int	tm_wday;
  int	tm_yday;
  int	tm_isdst;
};
typedef uint32_t time_t;

static int isleap(unsigned yr)
{
   return yr % 400 == 0 || (yr % 4 == 0 && yr % 100 != 0);
}

static unsigned months_to_days(unsigned month)
{
   return (month * 3057 - 3007) / 100;
}

static long years_to_days (unsigned yr)
{
   return yr * 365L + yr / 4 - yr / 100 + yr / 400;
}

static long ymd_to_scalar(unsigned yr, unsigned mo, unsigned day)
{
   long scalar;

   scalar = day + months_to_days(mo);
   if ( mo > 2 )                         /* adjust if past February */
      scalar -= isleap(yr) ? 1 : 2;
   yr--;
   scalar += years_to_days(yr);
   return (scalar);
}

static void scalar_to_ymd(long scalar,
                          unsigned *pyr,
                          unsigned *pmo,
                          unsigned *pday)
{
   unsigned n;                /* compute inverse of years_to_days() */

   n = (unsigned)((scalar * 400L) / 146097L);
   while (years_to_days(n) < scalar)
   {
      n++;
   }
   for ( n = (unsigned)((scalar * 400L) / 146097L); years_to_days(n) < scalar; )
      n++;                          /* 146097 == years_to_days(400) */
   *pyr = n;
   n = (unsigned)(scalar - years_to_days(n-1));
   if ( n > 59 ) {                       /* adjust if past February */
      n += 2;
      if ( isleap(*pyr) )
         n -= n > 62 ? 1 : 2;
   }
   *pmo = (n * 100 + 3007) / 3057;  /* inverse of months_to_days() */
   *pday = n - months_to_days(*pmo);
   return;
}

struct tm *gmtime(const time_t *timer)
{
    unsigned yr, mo, da;
    unsigned long secs;
    unsigned long days;

    days = *timer / (60L*60*24);
    secs = *timer % (60L*60*24);
    scalar_to_ymd(days + ymd_to_scalar(1970, 1, 1), &yr, &mo, &da);
    tms.tm_year = yr - 1900;
    tms.tm_mon = mo - 1;
    tms.tm_mday = da;
    tms.tm_yday = (int)(ymd_to_scalar(tms.tm_year + 1900, mo, da)
                  - ymd_to_scalar(tms.tm_year + 1900, 1, 1));
    tms.tm_wday = dow(tms.tm_year + 1900, mo, da);
    tms.tm_isdst = -1;
    tms.tm_sec = (int)(secs % 60);
    secs /= 60;
    tms.tm_min = (int)(secs % 60);
    secs /= 60;
    tms.tm_hour = (int)secs;
    return (&tms);
}

time_t my_mktime(struct tm *timeptr)
{
    time_t tt;

    if ((timeptr->tm_year < 70) || (timeptr->tm_year > 170)) /* unsigned long (int32) can hold 130+ years worth of seconds */
    {
        tt = (time_t)-1;
    }
    else
    {
        tt = ymd_to_scalar(timeptr->tm_year + 1900,
                           timeptr->tm_mon + 1,
                           timeptr->tm_mday)
             - ymd_to_scalar(1970, 1, 1);
        tt = tt * 24 + timeptr->tm_hour;
        tt = tt * 60 + timeptr->tm_min;
        tt = tt * 60 + timeptr->tm_sec;
    }
    return (tt);
}

uint32_t BCDToInt(uint8_t *Src, uint8_t Len)
{
	uint32_t Result = 0;
	uint8_t Temp, i;
	for (i = 0; i < Len; i++)
	{
		Result *= 100;
		Temp = (Src[i] >> 4) * 10 + (Src[i] & 0x0f);
		Result += Temp;
	}
	return Result;
}

void ReverseBCD(uint8_t *Src, uint8_t *Dst, uint32_t Len)
{
	uint32_t i;
	for (i = 0; i < Len; i++)
	{
		Dst[i] = (Src[i] >> 4) | (Src[i] << 4);
	}
}

uint16_t AsciiToGsmBcd(int8_t *pNumber, uint8_t nNumberLen, uint8_t *pBCD)
{
	uint8_t Tmp;
	uint32_t i;
	uint32_t nBcdSize = 0;
	uint8_t *pTmp     = pBCD;

	if (!pNumber|| !pBCD)
		return 0;

	memset(pBCD, 0, nNumberLen >> 1);

	for (i = 0; i < nNumberLen; i++)
	{
		switch (*pNumber)
		{

		case '*':
			Tmp = (int8_t)0x0A;
			break;

		case '#':
			Tmp = (int8_t)0x0B;
			break;

		case 'p':
			Tmp = (int8_t)0x0C;
			break;

		default:

			if (IsDigit((*pNumber)))
			{

				Tmp = (int8_t)(*pNumber - '0');
			}
			else
			{
				return 0;
			}
			break;
		}

		if (i % 2)
		{
			*pTmp++ |= (Tmp << 4) & 0xF0;
		}
		else
		{
			*pTmp = (int8_t)(Tmp & 0x0F);
		}

		pNumber++;
	}

	nBcdSize = nNumberLen >> 1;

	if (i % 2)
	{
		*pTmp |= 0xF0;
		nBcdSize += 1;
	}

	return nBcdSize;
}

uint8_t XorCheck(void *Src, uint32_t Len, uint8_t CheckStart)
{
	uint8_t Check = CheckStart;
	uint8_t *Data = (uint8_t *)Src;
	uint32_t i;
	for (i = 0; i < Len; i++)
	{
		Check ^= Data[i];
	}
	return Check;
}

uint32_t HexToAscii(uint8_t *Src, uint32_t Len, uint8_t *Dst)
{
	uint32_t i = 0;
	uint32_t j = 0;
	uint8_t hex_temp;
	while (i < Len) {
		hex_temp = (Src[i] & 0xf0) >> 4;
		if (hex_temp >= 10) {
			Dst[j++] = hex_temp - 10 + 'A';
		}
		else {
			Dst[j++] = hex_temp + '0';
		}
		hex_temp = Src[i++] & 0x0f;
		if (hex_temp >= 10) {
			Dst[j++] = hex_temp - 10 + 'A';
		}
		else {
			Dst[j++] = hex_temp + '0';
		}

	}
	return j;
}


int Net_TCPConnect(const char *Url, uint16_t Port)
{
	ip_addr_t *IP;
    struct hostent *hostentP = NULL;
    char *ipAddr = NULL;
    int connErr;
    struct sockaddr_in TCPServerAddr;
	int32_t Socketfd;

    //获取域名ip信息
    hostentP = gethostbyname(Url);
    if (!hostentP)
    {
    	DBG("gethostbyname %s fail", Url);
        return -1;
    }
    // 将ip转换成字符串
    ipAddr = ipaddr_ntoa((const ip_addr_t *)hostentP->h_addr_list[0]);

    DBG("gethostbyname %s ip %s", Url, ipAddr);
    IP = (ip_addr_t *)hostentP->h_addr_list[0];

	Socketfd = socket(AF_INET,SOCK_STREAM,0);
    if (Socketfd < 0)
    {
    	DBG("create tcp socket error");
        return -1;
    }

    memset(&TCPServerAddr, 0, sizeof(TCPServerAddr)); // 初始化服务器地址
    TCPServerAddr.sin_family = AF_INET;
    TCPServerAddr.sin_port = htons((unsigned short)Port);
    TCPServerAddr.sin_addr.s_addr = IP->addr;
    connErr = connect(Socketfd, (const struct sockaddr *)&TCPServerAddr, sizeof(struct sockaddr));
    if (connErr < 0)
    {
    	DBG("tcp connect error %d", socket_errno(Socketfd));
        close(Socketfd);
        return -1;
    }
    DBG("socket %d tcp connect success", Socketfd);
    return Socketfd;
}

int Net_UDPConnect(const char *Url, uint16_t Port, struct sockaddr_in *udp_server_addr)
{
	ip_addr_t *IP;
    struct hostent *hostentP = NULL;
    char *ipAddr = NULL;
	int32_t Socketfd;

    //获取域名ip信息
    hostentP = gethostbyname(Url);
    if (!hostentP)
    {
    	DBG("gethostbyname %s fail", Url);
        return -1;
    }
    // 将ip转换成字符串
    ipAddr = ipaddr_ntoa((const ip_addr_t *)hostentP->h_addr_list[0]);

    DBG("gethostbyname %s ip %s", Url, ipAddr);
    IP = (ip_addr_t *)hostentP->h_addr_list[0];

    udp_server_addr->sin_family = AF_INET;
    udp_server_addr->sin_addr.s_addr = IP->addr;
    udp_server_addr->sin_port = htons((unsigned short)Port);
	Socketfd = socket(AF_INET,SOCK_DGRAM,0);
    if (Socketfd < 0)
    {
    	DBG("create UDP socket error");
        return -1;
    }

    return Socketfd;
}


void LUAT_LBSTx(int SocketID, struct sockaddr_in *udp_server_addr)
{
	uint8_t TempBuf[256];
	uint32_t Pos = 0;
	uint8_t IMEI[8];
	uint16_t MCC = AGPSCtrl.MCC;
	uint8_t MNC = AGPSCtrl.MNC;
	uint8_t i,j;
	if (MNC == 4)
	{
		MNC = 0;
	}

	AsciiToGsmBcd(AGPSCtrl.IMEI, 15, IMEI);
	MCC = htons(MCC);
	TempBuf[Pos++] = strlen(LUAT_PRODUCTKEY);
	memcpy(TempBuf + Pos, LUAT_PRODUCTKEY, TempBuf[0]);
	Pos += TempBuf[0];
	TempBuf[Pos++] = 0x04;
	memcpy(TempBuf + Pos, IMEI, 8);
	Pos += 8;
	TempBuf[Pos++] = AGPSCtrl.LBSInfo.LACNum;
	for(i = 0; i < AGPSCtrl.LBSInfo.LACNum; i++)
	{
		memcpy(&TempBuf[Pos], AGPSCtrl.LBSInfo.LAC[i].LAC_BE, 2);
		Pos += 2;
		memcpy(&TempBuf[Pos], &MCC, 2);
		Pos += 2;
		TempBuf[Pos++] = MNC;
		for (j = 0; j < AGPSCtrl.LBSInfo.LAC[i].CINum; j++)
		{
			memcpy(&TempBuf[Pos], &AGPSCtrl.LBSInfo.LAC[i].CI[j], 3);
			if (!j)
			{
				TempBuf[Pos] |= (AGPSCtrl.LBSInfo.LAC[i].CINum - 1) << 5;
			}
			Pos += 3;
		}
	}
	for(i = 0; i < AGPSCtrl.LBSInfo.LACNum; i++)
	{
		DBG("LAC %d %02x%02x %d", i, AGPSCtrl.LBSInfo.LAC[i].LAC_BE[0],
				AGPSCtrl.LBSInfo.LAC[i].LAC_BE[1], AGPSCtrl.LBSInfo.LAC[i].CINum);
		for(j = 0; j < AGPSCtrl.LBSInfo.LAC[i].CINum; j++)
		{
			DBG("CI %d %02x%02x %d", j, AGPSCtrl.LBSInfo.LAC[i].CI[j].CI_BE[0],
					AGPSCtrl.LBSInfo.LAC[i].CI[j].CI_BE[1],
					AGPSCtrl.LBSInfo.LAC[i].CI[j].CSQ);
		}
	}
	//DBG_INFO("%d", Pos);
	//__HexTrace(LBSCtrl.TempBuf, Pos);
	sendto(SocketID, TempBuf, Pos, 0, (struct sockaddr*)udp_server_addr, sizeof(struct sockaddr));
}

int32_t LBS_ReceiveAnalyze(Buffer_Struct *Buf)
{
	uint32_t Lat, Lgt;
	int32_t Error;
	int32_t fromlen;
	int i = 0;
	struct tm time, *utc;
	time_t tt;
	T_AMOPENAT_SYSTEM_DATETIME Datetime;
	if (Buf->Data[i] != 0)
	{
		DBG("%d", Buf->Data[i]);
		return -1;
	}
	else
	{
		ReverseBCD(Buf->Data + 1, Buf->Data + 1, 10);
		if ( (Buf->Data[1] == 0xff) || (Buf->Data[6] == 0xff) )
		{
			//Dump)(Buf->Data, 11);
		}
		else
		{
			for (i = 1; i <= 10; i++)
			{
				if ( ((Buf->Data[i] & 0xf0) >> 4) >= 10 )
				{
					Buf->Data[i] &= 0x0f;
				}

				if ( ((Buf->Data[i] & 0x0f) >> 0) >= 10 )
				{
					Buf->Data[i] &= 0xf0;
				}
			}
			Lat = BCDToInt(Buf->Data + 1, 5);
			Lgt = BCDToInt(Buf->Data + 6, 5);
			AGPSCtrl.LatDegree = Lat / 10000000;
			AGPSCtrl.LatPoint = Lat % 10000000;
			AGPSCtrl.LgtDegree = Lgt / 10000000;
			AGPSCtrl.LgtPoint = Lgt % 10000000;
			i = 11;
			DBG("%d,%d,%d", Buf->Data[11], Buf->Data[12] , Buf->Data[13]);
			time.tm_year = Buf->Data[i];
			time.tm_year = time.tm_year + 100;
            //uMonth
            i++;
            time.tm_mon = Buf->Data[i] - 1;
            //uDay
            i++;
            time.tm_mday = Buf->Data[i];
            //uHour
            i++;
            time.tm_hour = Buf->Data[i];
            //uMinute
            i++;
            time.tm_min = Buf->Data[i];
            //uSecond
            i++;
            time.tm_sec = Buf->Data[i];
            tt = my_mktime(&time) - 8 * 3600;//转换成UTC时间
            utc = gmtime(&tt);
    		Datetime.nYear = utc->tm_year + 1900;
    		Datetime.nMonth = utc->tm_mon + 1;
    		Datetime.nDay = utc->tm_mday;
    		Datetime.nHour = utc->tm_hour;
    		Datetime.nMin = utc->tm_min;
    		Datetime.nSec = utc->tm_sec;
    		iot_os_set_system_datetime(&Datetime);
    		DBG( "%u.%07u,%u.%07u,0,%04u,%02u,%02u,%02u,%02u,%02u",
    				AGPSCtrl.LatDegree, AGPSCtrl.LatPoint,
    				AGPSCtrl.LgtDegree, AGPSCtrl.LgtPoint,
					Datetime.nYear, Datetime.nMonth, Datetime.nDay, Datetime.nHour,
					Datetime.nMin, Datetime.nSec);
		}
	}



	return 0;
}

int GK9501_AGPSNetRx(int HttpSocket, int LBSSocket, Buffer_Struct *Buf)
{
	int32_t Result;
    struct timeval tm;
    int MaxSocket = HttpSocket;
    fd_set ReadSet;
    FD_ZERO(&ReadSet);
    if (HttpSocket >= 0)
    {
    	FD_SET(HttpSocket, &ReadSet);
    }
    if (LBSSocket >= 0)
    {
    	FD_SET(LBSSocket, &ReadSet);
    	if (LBSSocket > HttpSocket)
    	{
    		MaxSocket = LBSSocket;
    	}
    }
    tm.tv_sec = 15;
    tm.tv_usec = 0;
    Result = select(MaxSocket + 1, &ReadSet, NULL, NULL, &tm);
    if(Result > 0)
    {
    	if (FD_ISSET(HttpSocket, &ReadSet))
    	{
    		Buf->Pos = recv(HttpSocket, Buf->Data, Buf->MaxLen, 0);
    		Result = HttpSocket;
    	}
    	else
    	{
    		Buf->Pos = recv(LBSSocket, Buf->Data, Buf->MaxLen, 0);
    		Result = LBSSocket;
    	}

        if(Buf->Pos <= 0)
        {
        	DBG("socket state %d", Buf->Pos);
            return -1;
        }
        DBG("%d,%d,%d,%d", HttpSocket, LBSSocket, Result, Buf->Pos);
		return Result;
    }
    return Result;

}

int GK9501_AGPSNetRun(void)
{

	int32_t Result, HeadLen;
	uint32_t RxLen;
	uint8_t LBSDone = 0;
	uint8_t ConnRetry;
	Buffer_Struct Buf;
	int32_t ContentLen = 0;
	int32_t Ret = -1;
	struct sockaddr_in udp_server_addr;
	int HttpSocket, LBSSocket;
	Buf.Data = iot_os_malloc(1600);
	if (!Buf.Data)
	{
		goto DL_END;
	}
	Buf.Pos = 0;
	Buf.MaxLen = 1600;
	ConnRetry = 0;
	HttpSocket = -1;
	LBSSocket = -1;

	LBSSocket = Net_UDPConnect(LBS_URL, LUAT_LBS_PORT, &udp_server_addr);
	if (LBSSocket < 0)
	{
		goto DL_END;
	}
	//通过LUAT的LBS服务获取大致的经纬度和时间
	//如果有其他手段获取到大致的经纬度和时间，比如保存了最近一次的经纬度，可以跳过LBS。
	LUAT_LBSTx(LBSSocket, &udp_server_addr);

DL_START:
	if (AGPSCtrl.FileBuf.Data)
	{
		iot_os_free(AGPSCtrl.FileBuf.Data);
		AGPSCtrl.FileBuf.Data = NULL;
	}
	AGPSCtrl.FileBuf.MaxLen = 0;
	AGPSCtrl.FileBuf.Pos = 0;
	SOCKET_CLOSE(HttpSocket);
	ConnRetry++;
	DBG("try %d times", ConnRetry);
	if (ConnRetry > 3)
	{
		goto DL_END;
	}
	HttpSocket = Net_TCPConnect(AGPS_HttpServer, AGPS_HTTP_PORT);

	if (HttpSocket >= 0)
	{

		if (send(HttpSocket, AGPS_HttpGetString, strlen(AGPS_HttpGetString), 0) < 0)
		{
			DBG("AGPS http tx error %d", socket_errno(HttpSocket));
			goto DL_START;
		}
DL_WAIT_HTTP:
		Result = GK9501_AGPSNetRx(HttpSocket, LBSSocket, &Buf);
		if (Result <= 0)
		{
			DBG("AGPS rx error");
			goto DL_START;
		}

		if (Result == HttpSocket)
		{
			Buf.Data[Buf.Pos] = 0;
			Result = HTTP_ResponseCode( Buf.Data );
			if (Result != HTTP_REASON_200)
			{
				DBG("%d", Result);
				goto DL_END;
			}
			ContentLen = HTTP_BodyLen(Buf.Data);
			DBG("%d", ContentLen);
			if (ContentLen < 0)
			{
				goto DL_END;
			}
			//fix less than 512 to 512
			AGPSCtrl.PacketTotal = ContentLen / 512 + ((ContentLen % 512)?1:0);
			DBG("need send packet %d", AGPSCtrl.PacketTotal);

			AGPSCtrl.FileBuf.Data = iot_os_malloc(AGPSCtrl.PacketTotal * 512);
			if (!AGPSCtrl.FileBuf.Data)
			{
				DBG("no mem!");
				goto DL_END;
			}
			AGPSCtrl.FileBuf.MaxLen = AGPSCtrl.PacketTotal * 512;
			AGPSCtrl.FileBuf.Pos = 0;
			DBG("fix buf len %d", AGPSCtrl.FileBuf.MaxLen);
			memset(AGPSCtrl.FileBuf.Data, 0, AGPSCtrl.FileBuf.MaxLen);
			HeadLen = HTTP_HeadLen(Buf.Data);
			if ( HeadLen < 0)
			{
				DBG("%d", HeadLen);
				goto DL_START;
			}

			if ( HeadLen != Buf.Pos)
			{
				if ( (HeadLen < Buf.Pos) )
				{
					DBG("data ready get %d", Buf.Pos - HeadLen);
					memcpy(AGPSCtrl.FileBuf.Data + AGPSCtrl.FileBuf.Pos, Buf.Data + HeadLen, Buf.Pos - HeadLen);
					AGPSCtrl.FileBuf.Pos += Buf.Pos - HeadLen;
				}
				else
				{
					DBG("data error");
					goto DL_START;
				}
			}
		}
		else if (Result == LBSSocket)
		{
			LBS_ReceiveAnalyze(&Buf);
			LBSDone = 1;
			goto DL_WAIT_HTTP;
		}
		else
		{
			DBG("!");
			goto DL_START;
		}


		while (AGPSCtrl.FileBuf.Pos < ContentLen)
		{
			Result = GK9501_AGPSNetRx(HttpSocket, LBSSocket, &Buf);
			if (Result <= 0)
			{
				DBG("AGPS rx error");
				goto DL_START;
			}

			if (Result == HttpSocket)
			{
				memcpy(AGPSCtrl.FileBuf.Data + AGPSCtrl.FileBuf.Pos, Buf.Data, Buf.Pos);
				AGPSCtrl.FileBuf.Pos += Buf.Pos;
				DBG("%u", AGPSCtrl.FileBuf.Pos);
			}
			else if (Result == LBSSocket)
			{
				LBS_ReceiveAnalyze(&Buf);
				LBSDone = 1;
			}
		}
		DBG("agps file download done!, %d, %d", ContentLen, AGPSCtrl.FileBuf.Pos);
		if (!LBSDone)
		{
			LUAT_LBSTx(LBSSocket, &udp_server_addr);
			Result = GK9501_AGPSNetRx(-1, LBSSocket, &Buf);
			if (Result <= 0)
			{
				Ret = -1;
				DBG("lbs rx error");
				goto DL_END;
			}
			LBS_ReceiveAnalyze(&Buf);
			LBSDone = 1;
		}

		Ret = 1;
	}
	else
	{
		goto DL_START;
	}
DL_END:
	iot_os_free(Buf.Data);
	SOCKET_CLOSE(HttpSocket);
	SOCKET_CLOSE(LBSSocket);
	return Ret;
}

int32_t GK9501_AGPSUartRun(void)
{
	uint8_t Data[128];
	Buffer_Struct RxBuf;
	int32_t Result = -1;
	T_AMOPENAT_UART_PARAM uartCfg;
	uint8_t ConnRetry = 0;
	uint8_t NoAckTime;

	AGPSCtrl.AGPSWork = 1;
	AGPSCtrl.Result = 0;
	memset(&uartCfg, 0, sizeof(T_AMOPENAT_UART_PARAM));
	uartCfg.baud = OPENAT_UART_BAUD_115200;
	uartCfg.dataBits = 8;
	uartCfg.stopBits = 1;
	uartCfg.parity = OPENAT_UART_NO_PARITY;
	uartCfg.flowControl = OPENAT_UART_FLOWCONTROL_NONE;
	uartCfg.txDoneReport = FALSE;
	uartCfg.uartMsgHande = NULL;
	iot_uart_close(OPENAT_UART_2);
	iot_uart_config(OPENAT_UART_2, &uartCfg);
	RxBuf.Data = Data;
	RxBuf.MaxLen = sizeof(Data);
AGPS_UART_START:
	AGPSCtrl.RxPos = 0;
	NoAckTime = 0;
	AGPSCtrl.AGPSState = GOKE_AGPS_START;
	GK9501_GPDStart();
AGPS_WAIT:
	DBG("%d, %d", AGPSCtrl.Result, NoAckTime);
	while ( (!AGPSCtrl.Result) && (NoAckTime < 5))
	{
		RxBuf.Pos = iot_uart_read(OPENAT_UART_2, Data, sizeof(Data), 1000);
		if (RxBuf.Pos)
		{
			GK9501_AGPSReceive(&RxBuf);
			if (AGPSCtrl.IsAck)
			{
				NoAckTime = 0;
			}
			else
			{
				NoAckTime++;
			}
		}
		else
		{
			DBG("%d", RxBuf.Pos);
			break;
		}
	}
	if (AGPSCtrl.Result > 0)
	{
		DBG("AGPS process ok!");
		Result = 0;
		iot_os_sleep(500);
		GK9501_SetTime();
		iot_os_sleep(500);
		GK9501_FastLocat();
		goto UART_DONE;
	}

	DBG("AGPS process fail!");
	iot_uart_close(OPENAT_UART_2);
    iot_pmd_poweron_ldo(OPENAT_LDO_POWER_CAM, 0);
	iot_os_sleep(100);
	iot_pmd_poweron_ldo(OPENAT_LDO_POWER_CAM, 1);
	iot_os_sleep(2000);
	iot_uart_config(OPENAT_UART_2, &uartCfg);
	ConnRetry++;
	if (ConnRetry < 3)
	{
		AGPSCtrl.PacketTotal = AGPSCtrl.FileBuf.MaxLen / 512 + ((AGPSCtrl.FileBuf.MaxLen % 512)?1:0);
		DBG("need send packet %d", AGPSCtrl.PacketTotal);
		goto AGPS_UART_START;
	}
UART_DONE:
	iot_os_sleep(100);
	iot_uart_close(OPENAT_UART_2);
	AGPSCtrl.reBootUart = 1;
	AGPSCtrl.AGPSWork = 0;
	return Result;
}

int32_t GK9501_AGPSReceive(Buffer_Struct *Buf)
{
	uint32_t i;
	int32_t Head = -1;
	uint8_t State = 0;
	uint16_t PackLen;
	uint16_t Ack;
	uint16_t MsgType;
	uint8_t Check;
	uint8_t Result;
	uint8_t Cmd[10];

	if (!Buf->Pos || (Buf->Pos > sizeof(AGPSCtrl.RxBuf)))
	{
		DBG("%d", Buf->Pos);
		return 1;
	}

	if ( (AGPSCtrl.RxPos + Buf->Pos) > sizeof(AGPSCtrl.RxBuf))
	{
		DBG("!");
		return 1;
	}
	DBG("%d %d", AGPSCtrl.RxPos, Buf->Pos);
	memcpy(AGPSCtrl.RxBuf + AGPSCtrl.RxPos, Buf->Data, Buf->Pos);
	AGPSCtrl.RxPos += Buf->Pos;
	if (AGPSCtrl.RxPos < 12)
	{
		DBG("%d", AGPSCtrl.RxPos);
		return 1;
	}
	i = 0;
	while (i < AGPSCtrl.RxPos)
	{
		switch (State)
		{
		case 0:
			if (AGPSCtrl.RxBuf[i] == 0xaa)
			{
				DBG("%d", i);
				Head = i;
				State = 1;
			}
			i++;
			break;
		case 1:
			if (AGPSCtrl.RxBuf[i] == 0xf0)
			{
				State = 2;
			}
			else
			{
				Head = -1;
				State = 0;
			}
			i++;
			break;
		case 2:
			Cmd[0] = AGPSCtrl.RxBuf[i];
			State = 3;
			i++;
			break;
		case 3:
			Cmd[1] = AGPSCtrl.RxBuf[i];
			State = 3;
			i++;
			memcpy(&PackLen, Cmd, 2);

			DBG("%d", PackLen);
			if (PackLen > (AGPSCtrl.RxPos - i + 4))
			{
				DBG("no enough data %d %d %d", PackLen, AGPSCtrl.RxPos, i);
				goto END;
			}
			if (PackLen == 0x000c)
			{
				memcpy(Cmd + 2, AGPSCtrl.RxBuf + i, 8);
				if ( (Cmd[8]!=0x0d) || (Cmd[9]!=0x0a) )
				{
					DBG("pack end error %02x %02x ", Cmd[8], Cmd[9]);
					goto FIND_END;
				}
				Check = XorCheck(Cmd, 7, 0);
				if (Cmd[7] != Check)
				{
					DBG("check error %02x %02x", Check, Cmd[7]);
					goto FIND_END;
				}
				memcpy(&Ack, &Cmd[2], 2);
				memcpy(&MsgType, &Cmd[4], 2);
				Result = Cmd[6];
				DBG("%d, %d, %d", Ack, MsgType, Result);
				AGPSCtrl.IsAck = 1;
				switch (AGPSCtrl.AGPSState)
				{
				case GOKE_AGPS_START:
					if ( (Ack == 1) && (MsgType == GOKE_CHANGE_BR_CMD) )
					{
						AGPSCtrl.AGPSState = GOKE_AGPS_SENDING;
						AGPSCtrl.PacketSn = 0;
						DBG("start sending data %d", AGPSCtrl.PacketSn);
						GK9501_SendCmd(AGPSCtrl.FileBuf.Data + AGPSCtrl.PacketSn * 512, 512, 1);
					}
					break;
				case GOKE_AGPS_SENDING:
					if ( (Ack == 3) && (MsgType == AGPSCtrl.PacketSn) && (Result == 1))
					{
						AGPSCtrl.PacketSn++;
						if (AGPSCtrl.PacketSn >= AGPSCtrl.PacketTotal)
						{
							DBG("send done");
							AGPSCtrl.AGPSState = GOKE_AGPS_SEND_DONE;
							GK9501_SendCmd(NULL, 0, 1);
						}
						else
						{
							DBG("send %d", AGPSCtrl.PacketSn);
							GK9501_SendCmd(AGPSCtrl.FileBuf.Data + AGPSCtrl.PacketSn * 512, 512, 1);
						}
					}
					break;
				case GOKE_AGPS_SEND_DONE:
					if ( (Ack == 3) && (MsgType == 65535) )
					{
						if (Result == 1)
						{
							AGPSCtrl.Result = 1;
							DBG("AGPS file ok!");
						}
						else
						{
							AGPSCtrl.Result = -1;
							DBG("AGPS file fail!");
						}

						AGPSCtrl.AGPSState = GOKE_AGPS_END;
						GK9501_SendCmd(NULL, 0, 1);

						AGPSCtrl.RxPos = 0;
						return 1;
					}
					break;
				default:
					break;
				}
			}
FIND_END:
			i += PackLen;
			Head = -1;
			break;
		default:
			DBG("!");
			AGPSCtrl.RxPos = 0;
			return 1;
			break;
		}
	}
END:
	DBG("%d %d %d",Head,i,AGPSCtrl.RxPos);
	if ( (Head != -1) && (i < AGPSCtrl.RxPos) )
	{
		memcpy(AGPSCtrl.RxBuf, AGPSCtrl.RxBuf + Head, AGPSCtrl.RxPos - Head);
		AGPSCtrl.RxPos = AGPSCtrl.RxPos - Head;
	}
	else
	{
		AGPSCtrl.RxPos = 0;
	}

	return 1;
}

void GK9501_SendCmd(uint8_t *Data, uint32_t Len, uint8_t IsBinMode)
{
	uint16_t TxLen, CmdLen;
	uint32_t BR = 115200;
	uint8_t CheckSum;
	CheckSum = 0;
	TxLen = 0;
	CmdLen = 0;


	if (IsBinMode)
	{
		AGPSCtrl.TxBuf[0] = 0xaa;
		AGPSCtrl.TxBuf[1] = 0xf0;
		switch (AGPSCtrl.AGPSState)
		{
		case GOKE_AGPS_SENDING:
			TxLen = 523;
			memcpy(AGPSCtrl.TxBuf + 2, &TxLen, 2);
			AGPSCtrl.TxBuf[4] = 0x66;
			AGPSCtrl.TxBuf[5] = 0x02;
			memcpy(AGPSCtrl.TxBuf + 6, &AGPSCtrl.PacketSn, 2);
			memcpy(AGPSCtrl.TxBuf + 8, Data, Len);
			break;
		case GOKE_AGPS_SEND_DONE:
			TxLen = 11;
			memcpy(AGPSCtrl.TxBuf + 2, &TxLen, 2);
			AGPSCtrl.TxBuf[4] = 0x66;
			AGPSCtrl.TxBuf[5] = 0x02;
			AGPSCtrl.TxBuf[6] = 0xff;
			AGPSCtrl.TxBuf[7] = 0xff;
			break;
		case GOKE_AGPS_END:
			TxLen = 14;
			memcpy(AGPSCtrl.TxBuf + 2, &TxLen, 2);
			AGPSCtrl.TxBuf[4] = 0x95;
			AGPSCtrl.TxBuf[5] = 0x00;
			AGPSCtrl.TxBuf[6] = 0x00;
			memcpy(AGPSCtrl.TxBuf + 7, &BR, 4);
			break;
		}
		AGPSCtrl.TxBuf[TxLen - 3] = XorCheck(AGPSCtrl.TxBuf + 2, TxLen - 5, 0);
		AGPSCtrl.TxBuf[TxLen - 2] = 0x0d;
		AGPSCtrl.TxBuf[TxLen - 1] = 0x0a;
		iot_uart_write(OPENAT_UART_2, AGPSCtrl.TxBuf, TxLen);
		//UART_Tx(__COM_UART__, AGPSCtrl.TxBuf, TxLen);
	}
	else
	{
		snprintf(AGPSCtrl.TxBuf + 1, GPS_LEN_MAX, GPS_CMD_HEAD"%s", Data);
		CmdLen = strlen(AGPSCtrl.TxBuf + 1);
		CheckSum = XorCheck(AGPSCtrl.TxBuf + 1, CmdLen, 0);
		AGPSCtrl.TxBuf[0] = '$';
		AGPSCtrl.TxBuf[1 + CmdLen] = '*';
		HexToAscii(&CheckSum, 1, AGPSCtrl.TxBuf + 2 + CmdLen);
		AGPSCtrl.TxBuf[4 + CmdLen] = '\r';
		AGPSCtrl.TxBuf[5 + CmdLen] = '\n';
		AGPSCtrl.TxBuf[6 + CmdLen] = 0;
		DBG("%s", AGPSCtrl.TxBuf);
		iot_uart_write(OPENAT_UART_2, AGPSCtrl.TxBuf, 6 + CmdLen);
	}

}

void GK9501_SetTime(void)
{
	uint8_t Cmd[128];
	T_AMOPENAT_SYSTEM_DATETIME DT;
	iot_os_get_system_datetime(&DT);
	if (DT.nYear < 2000)
	{
		DT.nYear += 2000;
	}

	snprintf(Cmd, 128, "634,%04u,%02u,%02u,%02u,%02u,%02u",
			DT.nYear, DT.nMonth, DT.nDay, DT.nHour, DT.nMin, DT.nSec);
	GK9501_SendCmd(Cmd, strlen(Cmd), 0);

}

void GK9501_FastLocat(void)
{
	uint8_t Cmd[128];
	T_AMOPENAT_SYSTEM_DATETIME DT;
	iot_os_get_system_datetime(&DT);
	if (DT.nYear < 2000)
	{
		DT.nYear += 2000;
	}

	snprintf(Cmd, 128, "635,%u.%07u,%u.%07u,0,%04u,%02u,%02u,%02u,%02u,%02u",
			AGPSCtrl.LatDegree, AGPSCtrl.LatPoint,
			AGPSCtrl.LgtDegree, AGPSCtrl.LgtPoint,
			DT.nYear, DT.nMonth, DT.nDay, DT.nHour, DT.nMin, DT.nSec);
	GK9501_SendCmd(Cmd, strlen(Cmd), 0);

}

void GK9501_GPDStart(void)
{
	GK9501_SendCmd(GOKE_GPD_START, strlen(GOKE_GPD_START), 0);
}
