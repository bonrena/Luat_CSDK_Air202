#ifndef __AGPS_H__
#define __AGPS_H__
#include "iot_types.h"
#define GPS_LEN_MAX (1024)
#define IsAlpha(c)      (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')))
#define IsHex(c)      (((c >= 'a') && (c <= 'f')) || ((c >= 'A') && (c <= 'F')))
#define IsDigit(c)        ((c >= '0') && (c <= '9'))
//带长度控制的缓冲结构
typedef struct
{
	uint8_t *Data;		//数据指针
	uint32_t Pos;		//包含的字节数
	uint32_t MaxLen;	//该缓冲区容量
}Buffer_Struct;

typedef struct
{
	uint8_t CSQ;
	uint8_t CI_BE[2];
}LBS_CIStruct;

typedef struct
{
	uint8_t LAC_BE[2];
	uint8_t MCC[2];
	uint8_t MNC[1];
	uint8_t CINum;
	LBS_CIStruct CI[7];
}LBS_LACStruct;

typedef struct
{
	uint8_t LACNum;
	LBS_LACStruct LAC[7];
}LBS_InfoStruct;

typedef struct
{
	LBS_InfoStruct LBSInfo;
	uint32_t LatDegree;
	uint32_t LatPoint;
	uint32_t LgtDegree;
	uint32_t LgtPoint;
	uint32_t RxPos;
	int32_t Result;
	Buffer_Struct FileBuf;
	Buffer_Struct UartBuf;
	uint16_t PacketSn;
	uint16_t PacketTotal;
	uint16_t MCC;
	uint8_t MNC;
	uint8_t IMEI[16];
	uint8_t RxBuf[GPS_LEN_MAX];
	uint8_t TxBuf[GPS_LEN_MAX];
	uint8_t NTPFlag;
	uint8_t LBSFlag;
	uint8_t AGPSWork;
	uint8_t AGPSState;
	uint8_t Power;
	uint8_t ActiveFlag;
	uint8_t reBootUart;
	uint8_t IsAck;
}AGPS_CtrlStruct;
extern AGPS_CtrlStruct AGPSCtrl;

int32_t GK9501_AGPSUartRun(void);
int GK9501_AGPSNetRun(void);
int32_t GK9501_AGPSReceive(Buffer_Struct *Buf);
#endif
