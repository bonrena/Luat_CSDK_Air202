#include "string.h"
#include "iot_debug.h"
#include "iot_sys.h"

#define CMD_CSQ   "AT+CSQ\r\n"
#define CMD_CGSN  "AT+CGSN\r\n"
#define CMD_ICCID   "AT+CCID\r\n"

static HANDLE g_s_vatTimer;
VOID vatAtIndHandle(UINT8 *pData, UINT16 length)
{
	iot_debug_print("[vat] AT RECV: %s", pData);
}

VOID vatTimerhandle(T_AMOPENAT_TIMER_PARAMETER *pParameter)
{   
    iot_os_sleep(2000);
    iot_debug_print("[vat] send %s", CMD_CSQ);
    iot_vat_sendATcmd(CMD_CSQ, strlen(CMD_CSQ));
    iot_os_start_timer(g_s_vatTimer, 2000);
    
    //���Ͳ�ѯIMEI��
    iot_os_sleep(2000);
    iot_debug_print("[vat] send %s", CMD_CGSN);
    iot_vat_sendATcmd(CMD_CGSN, strlen(CMD_CGSN));

    //���Ͳ�ѯSIM��CSQ����
    iot_os_sleep(2000);
    iot_debug_print("[vat] send %s", CMD_ICCID);
    iot_vat_sendATcmd(CMD_ICCID, strlen(CMD_ICCID));
}
VOID app_main(VOID)
{
    iot_debug_print("[vat] app_main");

    iot_vat_init(vatAtIndHandle);

	g_s_vatTimer = iot_os_create_timer(vatTimerhandle, NULL);
	
	iot_os_start_timer(g_s_vatTimer, 2000);
}