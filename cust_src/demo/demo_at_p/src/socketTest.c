#include "iot_xgd.h"

#define ddi_socket_print iot_debug_print


void ddi_socket_test(void)
{
	sock_addrs_t addrs[4];
	char readBuf[64];

	while(1)
	{
		ddi_sys_msleep(3000);
		
		// 解析域名百度
		s32 err = ddi_socket_get_addr(NULL, "www.baidu.com", addrs, 4, 0);

		// 创建socket
		s32 handle = ddi_socket_new(SOCK_TYPE_TCP);
		
		// 连接服务器"121.40.170.41", 12415 (注：回环服务器， 发什么收什么)
		s32 connEer = ddi_socket_connect(handle, "121.40.170.41", 12415, 0);
	
		U32 writeLen = ddi_socket_write(handle, "hello", strlen("hello"));

		// 接受hello
		memset(readBuf, 0, sizeof(readBuf));
		U32 readLen = ddi_socket_read(handle, readBuf, sizeof(readBuf), 1100);
		
		s32 cloEer = close(handle);
		ddi_socket_print("ddi_socket_test handle %d, connEer %d, err %d, readBuf %s", 
				handle, connEer, err, readBuf);

		
	}
}
