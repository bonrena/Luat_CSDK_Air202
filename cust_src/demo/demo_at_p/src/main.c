#include "string.h"
#include "iot_os.h"
#include "iot_debug.h"

typedef struct {
    UINT8 type;
}DEMO_SOCKET_MESSAGE; 
#define SOCKET_MSG_NETWORK_LINKED (1)
static HANDLE g_s_socket_task;

static void demo_NetworkIndCallBack(E_OPENAT_NETWORK_STATE state)
{
    T_OPENAT_NETWORK_CONNECT networkparam;
	static E_OPENAT_NETWORK_STATE oldState = OPENAT_NETWORK_DISCONNECT;
	DEMO_SOCKET_MESSAGE* msgptr = iot_os_malloc(sizeof(DEMO_SOCKET_MESSAGE));
  
	if (oldState == state)
	{
		return;
	}

	oldState = state;
	iot_debug_print("[ddi-main] network %d", state);

	// 已经注册上网了
    if (state == OPENAT_NETWORK_READY)
    {
    	memset(&networkparam, 0, sizeof(T_OPENAT_NETWORK_CONNECT));
    	memcpy(networkparam.apn, "CMIOT", strlen("CMIOT"));

		iot_debug_print("[ddi-main] iot_network_connect ");
		// 进行PDP激活
		iot_network_connect(&networkparam);
    }
	// pdp激活成功
	else if (state == OPENAT_NETWORK_LINKED)
	{
		// 发送消息测试ddi socket接口
		msgptr->type = SOCKET_MSG_NETWORK_LINKED;
        iot_os_send_message(g_s_socket_task, (PVOID)msgptr);
	}
}

static void demo_socket_task(PVOID pParameter)
{
    DEMO_SOCKET_MESSAGE*    msg;
    iot_debug_print("[ddi_task] wait network ready....");
    BOOL sock = FALSE;

    while(1)
    {
        iot_os_wait_message(g_s_socket_task, (PVOID)&msg);

        switch(msg->type)
        {
            case SOCKET_MSG_NETWORK_LINKED:
                iot_debug_print("[ddi_task] network connected");

				// 测试socket api接口
                ddi_socket_test();
                break;
        }

        iot_os_free(msg);
    }
}


VOID app_main(VOID)
{

	iot_debug_print("[CSDK DEMO_AT_P] return");
	return ;
    ddi_sys_msleep(1000);
	iot_debug_print("[CSDK DEMO_AT_P] COME IN");
	
	// 设置模块不会睡眠
	iot_pmd_exit_deepsleep();

	// 出现异常会死机， 方便调试
	iot_debug_set_fault_mode(OPENAT_FAULT_HANG);

	// 设置网络回调， 等待注网上报后， 激活PDP
	iot_network_set_cb(demo_NetworkIndCallBack);
	
    //创建task用来处理socket
	g_s_socket_task = iot_os_create_task(demo_socket_task,
                        NULL,
                        4096,
                        5,
                        OPENAT_OS_CREATE_DEFAULT,
                        "demo_socket");

	// 测试base api接口
	ddi_base_test();
    
    iot_debug_print("FS TEST START");
    ddi_sys_msleep(500);
    ddi_fs_test(); 
}
